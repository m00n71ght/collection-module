<form action="" method="GET">
    <div class="sort_coll_container">
        <div class="search_sort">
            <input type="search" name="search" class="search_input_sort font_color" placeholder="{{ trans('front.search') }}" aria-label="{{ trans('front.search input') }}">
            <span class="icon-search font_color"></span>
        </div>

        <div class="sort_title">
            <h2>{{ trans('front.filter by') }}</h2>
        </div>

        <div class="sort_item_container dropleft">
            <div class="sort_head bg_color font_color" data-offset="40" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" data-sort aria-label="Розділ колекції" role="button" tabindex="0">
                <p>{{ trans('front.collection.section') }}</p>
                <span class="icon-arrow_down"></span>
            </div>

            <div class="sort_body dropdown-menu font_color bg_color" role="menu" aria-label="menu">
                <div class="sort_body_flex mod1">
                    @foreach($sections as $section)
                        <div class="checkbox_item">
                            <input type="checkbox" id="section[{{$section->id}}]"
                                   class="check_input" aria-label="{{$section->name}}"
                                   name="section[{{$section->id}}]" {{(array_key_exists($section->id, $selected['section'])) ? 'checked' : ''}}>
                            <label for="section[{{$section->id}}]">{{$section->name}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="sort_item_container dropleft">
            <div class="sort_head bg_color font_color" data-toggle="dropdown" data-sort aria-label="Розділ колекції" role="button" tabindex="0">
                <p>{{ trans('front.collection.place') }}</p>
                <span class="icon-arrow_down"></span>
            </div>
            <div class="sort_body dropdown-menu font_color bg_color">
                <div class="sort_body_flex type1">
                    @foreach($places as $place)
                        <div class="checkbox_item">
                            <input type="checkbox" id="place[{{$place->id}}]"
                                   class="check_input" aria-label="{{$place->name}}"
                                   name="place[{{$place->id}}]" {{(array_key_exists($place->id, $selected['place'])) ? 'checked' : ''}}>
                            <label for="place[{{$place->id}}]">{{$place->name}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="sort_item_container dropleft">
            <div class="sort_head bg_color font_color" data-toggle="dropdown" data-sort aria-label="Тип предмету / Матеріал" role="button" tabindex="0">
                <p>{{ trans('front.collection.type') }}</p>
                <span class="icon-arrow_down"></span>
            </div>
            <div class="sort_body dropdown-menu type2 font_color bg_color">
                <div class="sort_body_flex type2">
                    @foreach($types as $type)
                        <div class="checkbox_item">
                            <input type="checkbox" id="type[{{$type->id}}]"
                                   class="check_input" aria-label="{{$type->name}}"
                                   name="type[{{$type->id}}]" {{(array_key_exists($type->id, $selected['type'])) ? 'checked' : ''}}>
                            <label for="type[{{$type->id}}]">{{$type->name}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="sort_item_container dropdown">
            <div class="sort_head bg_color font_color" data-toggle="dropdown" aria-label="Час створення" role="button" tabindex="0">
                <p>{{ trans('front.collection.date') }}</p>
                <span class="icon-arrow_down"></span>
            </div>
            <div class="sort_body dropdown-menu type1 font_color bg_color">
                <div class="sort_body_flex type2">
                    @foreach($dates as $date)
                        <div class="checkbox_item">
                            <input type="checkbox" id="date[{{$date->id}}]"
                                   class="check_input" aria-label="{{$date->name}}"
                                   name="date[{{$date->id}}]" {{(array_key_exists($date->id, $selected['date'])) ? 'checked' : ''}}>
                            <label for="date[{{$date->id}}]">{{$date->name}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="button_container">
            <input type="submit" class="button_submit bg_color font_color" value="{{ trans('front.apply') }}" aria-label="{{ trans('front.apply') }}">
        </div>

    </div>
</form>
