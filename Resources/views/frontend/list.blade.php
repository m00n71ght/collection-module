<div class="selected_container type1">
    <div class="row">
        <div class="col-12 col-md-6">
            <div class="row">
                <div class="col-12 col-lg-6">
                    @foreach($items[0] as $item)
                        @include('collection::frontend.partials.item')
                    @endforeach
                </div>
                <div class="col-12 col-lg-6">
                    @foreach($items[1] as $item)
                        @include('collection::frontend.partials.item')
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="row">
                <div class="col-12 col-lg-6">
                    @foreach($items[2] as $item)
                        @include('collection::frontend.partials.item')
                    @endforeach
                </div>
                <div class="col-12 col-lg-6">
                    @foreach($items[3] as $item)
                        @include('collection::frontend.partials.item')
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
