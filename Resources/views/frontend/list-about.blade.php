<section>
    <div class="about_collection bg_color">
        <div class="content_container">
            @foreach($items as $item)
                <div class="store_item type4 collection_it_about font_color bg_color">
                    <div class="item_foto">
                        <img src="{!! $item->image_url !!}" alt="{{ $item->alt_text }}" title="{{ $item->alt_text }}">
                    </div>
                    <div class="store_info color2 font_color">
                        <div class="store_info_name about_collection_info_name type2">
                            <h2>{{ $item->name }}</h2>
                            <p>{!! $item->short_text !!}</p>

                            <a href="#worK_text_link" role="button" data-more class="worK_text_link type2 font_color" aria-label="читати більше">{{ trans('front.read more') }}..</a>

                            <div class="collect_btn_container">
                                <a href="{!! $item->pdf_link !!}" class="download_btn font_color bg_color" role="button" aria-label="PDF" title="PDF">PDF</a>
                            </div>
                        </div>
                    </div>

                    <div class="item_sub_text text_more color2 font_color">
                        {!! $item->remain_text !!}

                        <a href="#worK_text_link" role="button" data-hide class="worK_text_link type2 font_color" aria-label="{{ trans('front.hide') }}">{{ trans('front.hide') }}</a>

                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
