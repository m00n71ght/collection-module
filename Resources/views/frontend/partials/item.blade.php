<div class="all_coll_item bg_color">
    <a href="{{ route('collection.item', $item->slug) }}">
    <img src="{!! $item->image_url !!}" alt="{{$item->alt_text}}">
    <div class="selected_item_info type1 font_color bg_color">
        <p>{{$item->name}}</p>
    </div>
    </a>
</div>
