<div class="form-group {{ $errors->has("active") ? ' has-error' : '' }}">
    {!! Form::checkbox("active", 1, true, ["class" => "flat-blue", "placeholder" => trans('collection::collections.dict.form.active')]) !!}
    {!! Form::label("active", trans('collection::collections.dict.form.active')) !!}
    {!! $errors->first("name", '<span class="help-block">:message</span>') !!}
</div>
