<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('collection::collections.dict.form.name')) !!}
    {!! Form::text("{$lang}[name]", old("{$lang}[name]"), ["class" => "form-control", "placeholder" => trans('collection::collections.dict.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>