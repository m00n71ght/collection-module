<div class="modal fade" tabindex="-1" role="dialog" id="ImportModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ trans('collection::collections.dict.Import csv file') }}</h4>
            </div>
            {!! Form::open(['url' => URL::current() . '/import', 'method' => 'post', 'files' => true]) !!}
            <div class="modal-body">
                <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                    <label for="file">{{ trans('collection::collections.dict.select CSV file') }}</label>
                    <input type="file" name="file">
                    {!! $errors->first('file', '<span class="help-block">:message</span>') !!}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('core::core.button.cancel') }}</button>
                <button class="btn btn-primary" type="submit">{{ trans('collection::collections.dict.Import') }}</button>
            </div>
            {!! Form::close() !!}
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@push('js-stack')
    <?php if ($errors->has('file')): ?>
    <script>
        $( document ).ready(function() {
            $('#ImportModal').modal('show');
        });
    </script>
    <?php endif; ?>
@endpush
