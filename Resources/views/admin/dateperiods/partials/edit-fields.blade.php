<div class="form-group {{ $errors->has("active") ? ' has-error' : '' }}">
    <input type="hidden" value="0" name="active">
    {!! Form::checkbox("active", 1, $dateperiod->active, ["class" => "flat-blue", "placeholder" => trans('collection::collections.dict.form.active')]) !!}
    {!! Form::label("active", trans('collection::collections.dict.form.active')) !!}
    {!! $errors->first("name", '<span class="help-block">:message</span>') !!}
</div>
