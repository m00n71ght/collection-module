{!! Form::normalSelect('section_id', trans('collection::items.form.section'), $errors, $sections, null, ['class' => '']) !!}
{!! Form::normalSelect('type_id', trans('collection::items.form.type'), $errors, $types, null, ['class' => '']) !!}
{!! Form::normalSelect('dateperiod_id', trans('collection::items.form.dateperiod'), $errors, $date_periods, null, ['class' => '']) !!}
{!! Form::normalSelect('origincountry_id', trans('collection::items.form.origincountry'), $errors, $origin_countries, null, ['class' => '']) !!}

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaMultiple('collectionItemImage', null, null, trans('collection::items.form.image'))
    </div>
</div>

{{--<div class="row">--}}
    {{--<div class="col-md-3 col-sm-4">--}}
        {{--@mediaSingle('collectionItemPdf', null, null, trans('collection::items.form.pdf'))--}}
    {{--</div>--}}
{{--</div>--}}

<div class="form-group {{ $errors->has("featured") ? ' has-error' : '' }}">
    {!! Form::checkbox("featured", 1, false, ["class" => "flat-blue", "placeholder" => trans('collection::collections.dict.form.featured')]) !!}
    {!! Form::label("featured", trans('collection::items.form.featured')) !!}
    {!! $errors->first("name", '<span class="help-block">:message</span>') !!}
</div>
