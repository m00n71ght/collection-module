{!! Form::normalSelect('section_id', trans('collection::items.form.section'), $errors, $sections, $item, ['class' => '']) !!}
{!! Form::normalSelect('type_id', trans('collection::items.form.type'), $errors, $types, $item, ['class' => '']) !!}
{!! Form::normalSelect('dateperiod_id', trans('collection::items.form.dateperiod'), $errors, $date_periods, $item, ['class' => '']) !!}
{!! Form::normalSelect('origincountry_id', trans('collection::items.form.origincountry'), $errors, $origin_countries, $item, ['class' => '']) !!}

<div class="row">
    <div class="col-md-3 col-sm-4">
        @mediaMultiple('collectionItemImage', $item, null, trans('collection::items.form.image'))
    </div>
</div>

{{--<div class="row">--}}
    {{--<div class="col-md-3 col-sm-4">--}}
        {{--@mediaSingle('collectionItemPdf', $item, null, trans('collection::items.form.pdf'))--}}
    {{--</div>--}}
{{--</div>--}}


<div class="form-group {{ $errors->has("featured") ? ' has-error' : '' }}">
    <input type="hidden" value="0" name="featured">
    {!! Form::checkbox("featured", 1, $item->featured, ["class" => "flat-blue", "placeholder" => trans('collection::collections.dict.form.featured')]) !!}
    {!! Form::label("featured", trans('collection::items.form.featured')) !!}
    {!! $errors->first("name", '<span class="help-block">:message</span>') !!}
</div>
