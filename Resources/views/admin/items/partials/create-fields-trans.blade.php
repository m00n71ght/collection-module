<div class="form-group {{ $errors->has("{$lang}[author]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[author]", trans('collection::items.form.author')) !!}
    {!! Form::text("{$lang}[author]", old("{$lang}[author]"), ["class" => "form-control", "placeholder" => trans('collection::items.form.author')]) !!}
    {!! $errors->first("{$lang}[author]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('collection::items.form.name')) !!}
    {!! Form::text("{$lang}[name]", old("{$lang}[name]"), ["class" => "form-control", "placeholder" => trans('collection::items.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[slug]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[slug]", trans('collection::items.form.slug')) !!}
    {!! Form::text("{$lang}[slug]", old("{$lang}[slug]"), ["class" => "form-control", "placeholder" => trans('collection::items.form.slug')]) !!}
    {!! $errors->first("{$lang}[slug]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[material]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[material]", trans('collection::items.form.material')) !!}
    {!! Form::text("{$lang}[material]", old("{$lang}[material]"), ["class" => "form-control", "placeholder" => trans('collection::items.form.material')]) !!}
    {!! $errors->first("{$lang}[material]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[inventory_number]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[inventory_number]", trans('collection::items.form.inventory_number')) !!}
    {!! Form::text("{$lang}[inventory_number]", old("{$lang}[inventory_number]"), ["class" => "form-control", "placeholder" => trans('collection::items.form.inventory_number')]) !!}
    {!! $errors->first("{$lang}[inventory_number]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[size]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[size]", trans('collection::items.form.size')) !!}
    {!! Form::text("{$lang}[size]", old("{$lang}[size]"), ["class" => "form-control", "placeholder" => trans('collection::items.form.size')]) !!}
    {!! $errors->first("{$lang}[size]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[source]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[source]", trans('collection::items.form.source')) !!}
    {!! Form::text("{$lang}[source]", old("{$lang}[source]"), ["class" => "form-control", "placeholder" => trans('collection::items.form.source')]) !!}
    {!! $errors->first("{$lang}[source]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[creation_time]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[creation_time]", trans('collection::items.form.creation_time')) !!}
    {!! Form::text("{$lang}[creation_time]", old("{$lang}[creation_time]"), ["class" => "form-control", "placeholder" => trans('collection::items.form.creation_time')]) !!}
    {!! $errors->first("{$lang}[creation_time]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[place]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[place]", trans('collection::items.form.place')) !!}
    {!! Form::text("{$lang}[place]", old("{$lang}[place]"), ["class" => "form-control", "placeholder" => trans('collection::items.form.place')]) !!}
    {!! $errors->first("{$lang}[place]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[expositions]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[expositions]", trans('collection::items.form.expositions')) !!}
    {!! Form::text("{$lang}[expositions]", old("{$lang}[expositions]"), ["class" => "form-control", "placeholder" => trans('collection::items.form.expositions')]) !!}
    {!! $errors->first("{$lang}[expositions]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[publications]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[publications]", trans('collection::items.form.publications')) !!}
    {!! Form::text("{$lang}[publications]", old("{$lang}[publications]"), ["class" => "form-control", "placeholder" => trans('collection::items.form.publications')]) !!}
    {!! $errors->first("{$lang}[publications]", '<span class="help-block">:message</span>') !!}
</div>


<div class="form-group {{ $errors->has("{$lang}[description]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[description]", trans('collection::items.form.description')) !!}
    {!! Form::textarea("{$lang}[description]", old("{$lang}[description]"), ["class" => "form-control", 'rows' => 5, "placeholder" => trans('collection::items.form.description')]) !!}
    {!! $errors->first("{$lang}[description]", '<span class="help-block">:message</span>') !!}
</div>