<div class="form-group {{ $errors->has("{$lang}[author]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[author]", trans('collection::items.form.author')) !!}
    <?php $old = $item->hasTranslation($lang) ? $item->translate($lang)->author : '' ?>
    {!! Form::text("{$lang}[author]", old("{$lang}[author]", $old), ["class" => "form-control", "placeholder" => trans('collection::items.form.author')]) !!}
    {!! $errors->first("{$lang}[author]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[name]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[name]", trans('collection::items.form.name')) !!}
    <?php $old = $item->hasTranslation($lang) ? $item->translate($lang)->name : '' ?>
    {!! Form::text("{$lang}[name]", old("{$lang}[name]", $old), ["class" => "form-control", "placeholder" => trans('collection::items.form.name')]) !!}
    {!! $errors->first("{$lang}[name]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[slug]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[slug]", trans('collection::items.form.slug')) !!}
    <?php $old = $item->hasTranslation($lang) ? $item->translate($lang)->slug : '' ?>
    {!! Form::text("{$lang}[slug]", old("{$lang}[slug]", $old), ["class" => "form-control", "placeholder" => trans('collection::items.form.slug')]) !!}
    {!! $errors->first("{$lang}[slug]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[material]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[material]", trans('collection::items.form.material')) !!}
    <?php $old = $item->hasTranslation($lang) ? $item->translate($lang)->material : '' ?>
    {!! Form::text("{$lang}[material]", old("{$lang}[material]", $old), ["class" => "form-control", "placeholder" => trans('collection::items.form.material')]) !!}
    {!! $errors->first("{$lang}[material]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[inventory_number]") ? ' has-error' : '' }}">
    {!! Form::label("{$lang}[inventory_number]", trans('collection::items.form.inventory_number')) !!}
    <?php $old = $item->hasTranslation($lang) ? $item->translate($lang)->inventory_number : '' ?>
    {!! Form::text("{$lang}[inventory_number]", old("{$lang}[inventory_number]", $old), ["class" => "form-control", "placeholder" => trans('collection::items.form.inventory_number')]) !!}
    {!! $errors->first("{$lang}[inventory_number]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[size]") ? ' has-error' : '' }}">
    <?php $old = $item->hasTranslation($lang) ? $item->translate($lang)->size : '' ?>
    {!! Form::label("{$lang}[size]", trans('collection::items.form.size')) !!}
    {!! Form::text("{$lang}[size]", old("{$lang}[size]", $old), ["class" => "form-control", "placeholder" => trans('collection::items.form.size')]) !!}
    {!! $errors->first("{$lang}[size]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[source]") ? ' has-error' : '' }}">
    <?php $old = $item->hasTranslation($lang) ? $item->translate($lang)->source : '' ?>
    {!! Form::label("{$lang}[source]", trans('collection::items.form.source')) !!}
    {!! Form::text("{$lang}[source]", old("{$lang}[source]", $old), ["class" => "form-control", "placeholder" => trans('collection::items.form.source')]) !!}
    {!! $errors->first("{$lang}[source]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[creation_time]") ? ' has-error' : '' }}">
    <?php $old = $item->hasTranslation($lang) ? $item->translate($lang)->creation_time : '' ?>
    {!! Form::label("{$lang}[creation_time]", trans('collection::items.form.creation_time')) !!}
    {!! Form::text("{$lang}[creation_time]", old("{$lang}[creation_time]", $old), ["class" => "form-control", "placeholder" => trans('collection::items.form.creation_time')]) !!}
    {!! $errors->first("{$lang}[creation_time]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[place]") ? ' has-error' : '' }}">
    <?php $old = $item->hasTranslation($lang) ? $item->translate($lang)->place : '' ?>
    {!! Form::label("{$lang}[place]", trans('collection::items.form.place')) !!}
    {!! Form::text("{$lang}[place]", old("{$lang}[place]", $old), ["class" => "form-control", "placeholder" => trans('collection::items.form.place')]) !!}
    {!! $errors->first("{$lang}[place]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[expositions]") ? ' has-error' : '' }}">
    <?php $old = $item->hasTranslation($lang) ? $item->translate($lang)->expositions : '' ?>
    {!! Form::label("{$lang}[expositions]", trans('collection::items.form.expositions')) !!}
    {!! Form::text("{$lang}[expositions]", old("{$lang}[expositions]", $old), ["class" => "form-control", "placeholder" => trans('collection::items.form.expositions')]) !!}
    {!! $errors->first("{$lang}[expositions]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[publications]") ? ' has-error' : '' }}">
    <?php $old = $item->hasTranslation($lang) ? $item->translate($lang)->publications : '' ?>
    {!! Form::label("{$lang}[publications]", trans('collection::items.form.publications')) !!}
    {!! Form::text("{$lang}[publications]", old("{$lang}[publications]", $old), ["class" => "form-control", "placeholder" => trans('collection::items.form.publications')]) !!}
    {!! $errors->first("{$lang}[publications]", '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has("{$lang}[description]") ? ' has-error' : '' }}">
    <?php $old = $item->hasTranslation($lang) ? $item->translate($lang)->description : '' ?>
    {!! Form::label("{$lang}[description]", trans('collection::items.form.description')) !!}
    {!! Form::textarea("{$lang}[description]", old("{$lang}[description]", $old), ["class" => "form-control", 'rows' => 5, "placeholder" => trans('collection::items.form.description')]) !!}
    {!! $errors->first("{$lang}[description]", '<span class="help-block">:message</span>') !!}
</div>