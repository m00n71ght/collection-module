<?php

return [
    'list resource' => 'List types',
    'create resource' => 'Create types',
    'edit resource' => 'Edit types',
    'destroy resource' => 'Destroy types',
    'import resource' => 'Import types',
    'title' => [
        'types' => 'Type',
        'create type' => 'Create a type',
        'edit type' => 'Edit a type',
    ],
    'button' => [
        'create type' => 'Create a type',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    'imported' => 'Types successfully imported'
];
