<?php

return [
    'title' => [
        'collection' => 'Collection',
        'reference_book' => 'Reference Book'
    ],
    'dict' => [
        'form' => [
            'name' => 'Name',
            'active' => 'Active'
        ],
        'Import' => 'Import',
        'Import csv file' => 'Import CSV File',
        'select CSV file' => 'Select csv File',

    ]
];
