<?php

return [
    'list resource' => 'List items',
    'create resource' => 'Create items',
    'edit resource' => 'Edit items',
    'destroy resource' => 'Destroy items',
    'title' => [
        'items' => 'Item',
        'create item' => 'Create a item',
        'edit item' => 'Edit a item',
    ],
    'button' => [
        'create item' => 'Create a item',
    ],
    'table' => [
    ],
    'form' => [
        'name' => 'Name',
        'author' => 'Author',
        'material' => 'Material',
        'inventory_number' => 'Inventory #',
        'size' => 'Size',
        'source' => 'Source origin',
        'active' => 'Active',
        'section' => 'Section',
        'featured' => 'Featured',
        'dateperiod' => 'Date Period',
        'creation_time' => 'Creation Time',
        'type' => 'Type',
        'origincountry' => 'Origin Country',
        'image' => 'Picture',
        'alt_text' => 'Image alt text',
        'description' => 'Description',
        'slug' => 'Slug',
        'pdf' => 'PDF file',
        'place' => 'Place of origin',
        'expositions' => 'expositions',
        'publications' => 'publications'
    ],
    'frontend' => [
        'name' => 'Name',
        'author' => 'Author',
        'material' => 'Material',
        'inventory_number' => 'Inventory #',
        'size' => 'Size',
        'source' => 'Source origin',
        'active' => 'Active',
        'section' => 'Section',
        'featured' => 'Featured',
        'dateperiod' => 'Date Period',
        'type' => 'Type',
        'origincountry' => 'Origin Country',
        'image' => 'Picture',
        'alt_text' => 'Image alt text',
        'description' => 'Description',
        'creation_time' => 'Creation time',
        'place' => 'Place of origin',
        'expositions' => 'expositions',
        'publications' => 'publications'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
