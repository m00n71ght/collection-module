<?php

return [
    'list resource' => 'List dateperiods',
    'create resource' => 'Create dateperiods',
    'edit resource' => 'Edit dateperiods',
    'destroy resource' => 'Destroy dateperiods',
    'import resource' => 'Import dateperiods',
    'title' => [
        'dateperiods' => 'DatePeriod',
        'create dateperiod' => 'Create a dateperiod',
        'edit dateperiod' => 'Edit a dateperiod',
    ],
    'button' => [
        'create dateperiod' => 'Create a dateperiod',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    'imported' => 'Date periods successfully imported'
];
