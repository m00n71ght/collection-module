<?php

return [
    'list resource' => 'List origincountries',
    'create resource' => 'Create origincountries',
    'edit resource' => 'Edit origincountries',
    'destroy resource' => 'Destroy origincountries',
    'import resource' => 'Import origincountries',
    'title' => [
        'origincountries' => 'OriginCountry',
        'create origincountry' => 'Create a origincountry',
        'edit origincountry' => 'Edit a origincountry',
    ],
    'button' => [
        'create origincountry' => 'Create a origincountry',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    'imported' => 'Origin countries successfully imported'
];
