<?php

return [
    'author is required' => 'Author is required',
    'name is required' => 'Name is required',
    'slug is required' => 'Slug is required',
    'material is required' => 'Material is required',
    'size is required' => 'Size is required',
    'inventory_number is required' => 'Inventory number is required',
    'source is required' => 'Source is required',
];