<?php

return [
    'list resource' => 'List sections',
    'create resource' => 'Create sections',
    'edit resource' => 'Edit sections',
    'destroy resource' => 'Destroy sections',
    'import resource' => 'Import sections',
    'title' => [
        'sections' => 'Section',
        'create section' => 'Create a section',
        'edit section' => 'Edit a section',
    ],
    'button' => [
        'create section' => 'Create a section',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    'imported' => 'Sections successfully imported'
];
