<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionDatePeriodTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collection__dateperiod_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields
            $table->string('name', 100);

            $table->integer('dateperiod_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['dateperiod_id', 'locale']);
            $table->foreign('dateperiod_id')->references('id')->on('collection__dateperiods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collection__dateperiod_translations', function (Blueprint $table) {
            $table->dropForeign(['dateperiod_id']);
        });
        Schema::dropIfExists('collection__dateperiod_translations');
    }
}
