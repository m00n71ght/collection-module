<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddedForeignKeysToItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collection__items', function (Blueprint $table) {
            $table->boolean('featured')->default(0);

            $table->integer('section_id')->unsigned();
            $table->foreign('section_id')->references('id')->on('collection__sections')->onDelete('cascade');

            $table->integer('type_id')->unsigned();
            $table->foreign('type_id')->references('id')->on('collection__types')->onDelete('cascade');

            $table->integer('origincountry_id')->unsigned();
            $table->foreign('origincountry_id')->references('id')->on('collection__origincountries')->onDelete('cascade');

            $table->integer('dateperiod_id')->unsigned();
            $table->foreign('dateperiod_id')->references('id')->on('collection__dateperiods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collection__items', function (Blueprint $table) {
            $table->dropForeign(['section_id']);
            $table->dropForeign(['type_id']);
            $table->dropForeign(['origin_country_id']);
            $table->dropForeign(['date_period_id']);
            $table->dropColumn(['section_id', 'type_id', 'origin_country_id', 'date_period_id', 'featured']);
        });
    }
}
