<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionOriginCountryTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collection__origincountry_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields
            $table->string('name', 100);

            $table->integer('origincountry_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['origincountry_id', 'locale'], 'origincountry_locale_unique');
            $table->foreign('origincountry_id')->references('id')->on('collection__origincountries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collection__origincountry_translations', function (Blueprint $table) {
            $table->dropForeign(['origincountry_id']);
        });
        Schema::dropIfExists('collection__origincountry_translations');
    }
}
