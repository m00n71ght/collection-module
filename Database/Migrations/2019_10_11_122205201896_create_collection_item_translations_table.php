<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionItemTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collection__item_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields
            $table->string('author', 100)->nullable();
            $table->string('name', 100);
            $table->string('material', 40)->nullable();
            $table->string('size', 40)->nullable();
            $table->string('inventory_number', 20)->nullable();
            $table->text('place')->nullable();
            $table->text('expositions')->nullable();
            $table->text('publications')->nullable();
            $table->text('source')->nullable();
            $table->string('creation_time', 60);
            $table->integer('item_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['item_id', 'locale']);
            $table->foreign('item_id')->references('id')->on('collection__items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collection__item_translations', function (Blueprint $table) {
            $table->dropForeign(['item_id']);
        });
        Schema::dropIfExists('collection__item_translations');
    }
}
