<?php


namespace Modules\Collection\Presenters;

interface CollectionPresenterInterface
{
    public function all($featured);

    public function filters();

    public function item($slug);

    public function random();
}
