<?php


namespace Modules\Collection\Presenters;


use Modules\Collection\Repositories\DatePeriodRepository;
use Modules\Collection\Repositories\ItemRepository;
use Modules\Collection\Repositories\OriginCountryRepository;
use Modules\Collection\Repositories\SectionRepository;
use Modules\Collection\Repositories\TypeRepository;

abstract class AbstractCollectionPresenter implements CollectionPresenterInterface
{
    protected $items;
    /**
     * @var DatePeriodRepository
     */
    protected $dates;
    /**
     * @var OriginCountryRepository
     */
    protected $places;
    /**
     * @var SectionRepository
     */
    protected $sections;
    /**
     * @var TypeRepository
     */
    protected $types;

    public function __construct(ItemRepository $items, DatePeriodRepository $dates, OriginCountryRepository $places, SectionRepository $sections, TypeRepository $types)
    {
        $this->items = $items;
        $this->dates = $dates;
        $this->places = $places;
        $this->sections = $sections;
        $this->types = $types;
    }
}
