<?php


namespace Modules\Collection\Presenters;

use Illuminate\Support\Facades\View;

class CollectionPresenter extends AbstractCollectionPresenter implements CollectionPresenterInterface
{

    public function all($featured = false)
    {
        $template = 'collection::frontend.list';

        $items = app()->call([$this->items, 'search'], ['featured' => $featured])->all();

        $results = [];
        foreach ($items as $i => $item) {
            $results[$i % 4][] = $item;
        }
        $items = array_pad($results, 4, []);
        $view = View::make($template)->with(compact('items'));
        return $view->render();
    }

    public function about()
    {
        $template = 'collection::frontend.list-about';

        $items = $this->items->all();

        $view = View::make($template)->with(compact('items'));
        return $view->render();
    }

    public function filters()
    {
        $template = 'collection::frontend.filters';

        $selected = [
            'type' => [],
            'date' => [],
            'place' => [],
            'section' => []
        ];
        $parameters = [
            'dates' => $this->dates->allActive(),
            'places' => $this->places->allActive(),
            'sections' => $this->sections->allActive(),
            'types' => $this->types->allActive(),
            'selected' => array_merge($selected, request()->all())
        ];

        $view = View::make($template)->with($parameters);
        return $view->render();
    }

    public function item($slug)
    {
        $template = 'collection::frontend.item';

        $item = $this->items->findBySlug($slug);

        $view = View::make($template)->with(compact('item'));
        return $view->render();
    }

    public function random()
    {
        $items = $this->items->all();

        $random_item = $items->where('featured', false)->random();
        $random_featured = $items->where('featured', true)->random();

        return [$random_item, $random_featured];
    }
}
