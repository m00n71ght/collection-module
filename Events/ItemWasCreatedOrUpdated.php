<?php


namespace Modules\Collection\Events;


use Modules\Media\Contracts\StoringMedia;

class ItemWasCreatedOrUpdated implements StoringMedia
{
    private $item;
    private $data;

    public function __construct($item, $data)
    {
        $this->item = $item;
        $this->data = $data;
    }

    /**
     * Return the entity
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getEntity()
    {
        return $this->item;
    }

    /**
     * Return the ALL data sent
     * @return array
     */
    public function getSubmissionData()
    {
        return $this->data;
    }
}
