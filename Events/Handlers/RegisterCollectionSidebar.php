<?php

namespace Modules\Collection\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterCollectionSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('collection::collections.title.collection'), function (Item $item) {
                $item->icon('fa fa-magic');
                $item->weight(10);
                $item->authorize(
                     /* append */
                );
                $item->item(trans('collection::items.title.items'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.collection.item.create');
                    $item->route('admin.collection.item.index');
                    $item->authorize(
                        $this->auth->hasAccess('collection.items.index')
                    );
                });
                $item->item(trans('collection::sections.title.sections'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.collection.section.create');
                    $item->route('admin.collection.section.index');
                    $item->authorize(
                        $this->auth->hasAccess('collection.sections.index')
                    );
                });
                $item->item(trans('collection::collections.title.reference_book'), function(Item $item) {
                    $item->item(trans('collection::dateperiods.title.dateperiods'), function (Item $item) {
                        $item->icon('fa fa-copy');
                        $item->weight(0);
                        $item->append('admin.collection.dateperiod.create');
                        $item->route('admin.collection.dateperiod.index');
                        $item->authorize(
                            $this->auth->hasAccess('collection.dateperiods.index')
                        );
                    });
                    $item->item(trans('collection::origincountries.title.origincountries'), function (Item $item) {
                        $item->icon('fa fa-copy');
                        $item->weight(0);
                        $item->append('admin.collection.origincountry.create');
                        $item->route('admin.collection.origincountry.index');
                        $item->authorize(
                            $this->auth->hasAccess('collection.origincountries.index')
                        );
                    });
                    $item->item(trans('collection::types.title.types'), function (Item $item) {
                        $item->icon('fa fa-copy');
                        $item->weight(0);
                        $item->append('admin.collection.type.create');
                        $item->route('admin.collection.type.index');
                        $item->authorize(
                            $this->auth->hasAccess('collection.types.index')
                        );
                    });
                });
// append






            });
        });

        return $menu;
    }
}
