<?php

namespace Modules\Collection\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Collection\Entities\Traits\Activateable;

class Origincountry extends Model
{
    use Translatable, Activateable;

    protected $table = 'collection__origincountries';
    public $translatedAttributes = ['name'];
    protected $fillable = [
        'name',
        'active'
    ];
}
