<?php

namespace Modules\Collection\Entities;

use Illuminate\Database\Eloquent\Model;

class TypeTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];
    protected $table = 'collection__type_translations';
}
