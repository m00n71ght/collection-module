<?php

namespace Modules\Collection\Entities;

use Illuminate\Database\Eloquent\Model;

class ItemTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'author',
        'name',
        'slug',
        'material',
        'size',
        'inventory_number',
        'source',
        'description',
        'creation_time',
        'place',
        'expositions',
        'publications'
    ];
    protected $table = 'collection__item_translations';
}
