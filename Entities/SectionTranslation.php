<?php

namespace Modules\Collection\Entities;

use Illuminate\Database\Eloquent\Model;

class SectionTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];
    protected $table = 'collection__section_translations';
}
