<?php

namespace Modules\Collection\Entities;

use Illuminate\Database\Eloquent\Model;

class DateperiodTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];
    protected $table = 'collection__dateperiod_translations';
}
