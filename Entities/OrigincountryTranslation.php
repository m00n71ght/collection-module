<?php

namespace Modules\Collection\Entities;

use Illuminate\Database\Eloquent\Model;

class OrigincountryTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];
    protected $table = 'collection__origincountry_translations';
}
