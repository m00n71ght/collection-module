<?php


namespace Modules\Collection\Entities\Traits;


use Illuminate\Database\Eloquent\Builder;

trait Activateable
{
    public function scopeActive(Builder $query)
    {
        return $query->where('active', true);
    }
}
