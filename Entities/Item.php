<?php

namespace Modules\Collection\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Modules\Media\Support\Traits\MediaRelation;

class Item extends Model
{
    use Translatable, MediaRelation;

    protected $table = 'collection__items';
    public $translatedAttributes = [
        'author',
        'name',
        'slug',
        'material',
        'size',
        'inventory_number',
        'source',
        'description',
        'creation_time',
        'place',
        'expositions',
        'publications'
    ];
    protected $fillable = [
        'author',
        'name',
        'slug',
        'material',
        'size',
        'inventory_number',
        'source',
        'featured',
        'dateperiod_id',
        'origincountry_id',
        'type_id',
        'section_id',
        'description',
        'creation_time',
        'place',
        'expositions',
        'publications'
    ];

    protected $casts = [
        'featured' => 'boolean'
    ];

    public function dateperiod()
    {
        return $this->belongsTo(Dateperiod::class);
    }

    public function origincountry()
    {
        return $this->belongsTo(Origincountry::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    public function images()
    {
        return $this->files()->where('zone', 'collectionItemImage');
    }

    public function getImageUrlAttribute()
    {
        return $this->files()->where('zone', 'collectionItemImage')->first() ? $this->files()->where('zone', 'collectionItemImage')->first()->path : '';
    }

    public function getAltTextAttribute()
    {
        return $this->files()->where('zone', 'collectionItemImage')->first() ? $this->files()->where('zone', 'collectionItemImage')->first()->alt_attribute : '';
    }

    public function scopeFeatured(Builder $query)
    {
        return $query->where('featured', true);
    }

    public function getPdfLinkAttribute()
    {
        $file = $this->files()->where('zone', 'collectionItemPdf' . app()->getLocale())->first();
        if ($file === null) {
            return '';
        }
        return $file->path;
    }

    public function getShortTextAttribute()
    {
        return Str::limit($this['description'], 150);
    }

    public function getRemainTextAttribute()
    {
        return substr($this['description'], 150);
    }

    public function getFormattedTextAttribute()
    {
        $text = str_replace("\n", '</p><p>', $this['description']);

        return '<p>' . $text . '</p>';
    }

}
