<?php


namespace Modules\Collection\Importers;


use League\Csv\Reader;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ReferenceBookImporter
{
    private $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function import(UploadedFile $file)
    {
        $csv = Reader::createFromPath($file->getRealPath());
        $csv->detectDelimiterList(5, [',', ';']);
        $headers = $csv->fetchOne();
        $csv->setOffset(1);

        $csv->each(function ($row) use ($headers) {
            try {
                $row = array_combine($headers, $row);
            } catch (\Exception $e) {
                return true;
            }

            $data = [];
            foreach ($row as $locale => $name) {
                $data[$locale] = ['name' => $name];
            }

            if (method_exists($this->model, 'updateFromImport')) {
                $this->model->updateFromImport($data);
            };

            return true;
        });
    }
}
