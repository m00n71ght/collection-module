<?php

return [
    'collection.items' => [
        'index' => 'collection::items.list resource',
        'create' => 'collection::items.create resource',
        'edit' => 'collection::items.edit resource',
        'destroy' => 'collection::items.destroy resource',
    ],
    'collection.dateperiods' => [
        'index' => 'collection::dateperiods.list resource',
        'create' => 'collection::dateperiods.create resource',
        'edit' => 'collection::dateperiods.edit resource',
        'destroy' => 'collection::dateperiods.destroy resource',
        'import' => 'collection::dateperiods.import resource'
    ],
    'collection.origincountries' => [
        'index' => 'collection::origincountries.list resource',
        'create' => 'collection::origincountries.create resource',
        'edit' => 'collection::origincountries.edit resource',
        'destroy' => 'collection::origincountries.destroy resource',
        'import' => 'collection::origincountries.import resource'
    ],
    'collection.types' => [
        'index' => 'collection::types.list resource',
        'create' => 'collection::types.create resource',
        'edit' => 'collection::types.edit resource',
        'destroy' => 'collection::types.destroy resource',
        'import' => 'collection::types.import resource'
    ],
    'collection.sections' => [
        'index' => 'collection::sections.list resource',
        'create' => 'collection::sections.create resource',
        'edit' => 'collection::sections.edit resource',
        'destroy' => 'collection::sections.destroy resource',
        'import' => 'collection::sections.import resource'
    ],
// append






];
