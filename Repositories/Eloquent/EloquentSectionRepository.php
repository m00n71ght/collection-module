<?php

namespace Modules\Collection\Repositories\Eloquent;

use Modules\Collection\Repositories\SectionRepository;
use Modules\Collection\Repositories\Traits\Importable;
use Modules\Collection\Repositories\Traits\ReferenceBook;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentSectionRepository extends EloquentBaseRepository implements SectionRepository
{
    use Importable, ReferenceBook;
}
