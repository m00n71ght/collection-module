<?php

namespace Modules\Collection\Repositories\Eloquent;

use Modules\Collection\Repositories\Traits\Importable;
use Modules\Collection\Repositories\Traits\ReferenceBook;
use Modules\Collection\Repositories\TypeRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentTypeRepository extends EloquentBaseRepository implements TypeRepository
{
    use Importable, ReferenceBook;
}
