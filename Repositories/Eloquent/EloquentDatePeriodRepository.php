<?php

namespace Modules\Collection\Repositories\Eloquent;

use Modules\Collection\Repositories\DatePeriodRepository;
use Modules\Collection\Repositories\Traits\Importable;
use Modules\Collection\Repositories\Traits\ReferenceBook;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentDatePeriodRepository extends EloquentBaseRepository implements DatePeriodRepository
{
    use Importable, ReferenceBook;
}
