<?php

namespace Modules\Collection\Repositories\Eloquent;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Modules\Collection\Events\ItemWasCreatedOrUpdated;
use Modules\Collection\Repositories\ItemRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentItemRepository extends EloquentBaseRepository implements ItemRepository
{
    public function create($data)
    {
        foreach ($data as &$locale) {
            if (isset($locale['name']) && empty($locale['slug'])) {
                $locale['slug'] = Str::slug($locale['name'], '-');
            }
        }
        $item = parent::create($data);
        event(new ItemWasCreatedOrUpdated($item, $data));
        return $item;
    }

    public function update($model, $data)
    {
        foreach ($data as &$locale) {
            if (isset($locale['name']) && empty($locale['slug'])) {
                $locale['slug'] = Str::slug($locale['name'], '-');
            }
        }
        $item = parent::update($model, $data);
        event(new ItemWasCreatedOrUpdated($item, $data));
        return $item;
    }

    public function search(Request $request, $featured = false)
    {
        $query = $this->model->query();

        if (method_exists($this->model, 'translations')) {
            $query = $query->with('translations');
        }

        $query = $query
            ->when($featured, function(Builder $query) {
                $query->featured();
            })
            ->when($request->has('search'), function (Builder $query) use ($request) {
                $term = $request->get('search');
                $term = preg_replace('/\W/u', '', $term);
                $query->where(function(Builder $query) use ($term) {
                   $query->whereTranslationLike('name', "%$term%");
//                       ->orWhere('name', 'like', "%$term%");
                });
            })
            ->when($request->has('date'), function (Builder $query) use ($request) {
                $query->whereIn('dateperiod_id', array_keys($request->get('date')));
            })
            ->when($request->has('section'), function (Builder $query) use ($request) {
                $query->whereIn('section_id', array_keys($request->get('section')));
            })
            ->when($request->has('type'), function (Builder $query) use ($request) {
                $query->whereIn('type_id', array_keys($request->get('type')));
            })
            ->when($request->has('place'), function (Builder $query) use ($request) {
                $query->whereIn('origincountry_id', array_keys($request->get('place')));
            });


        $query = $query->orderBy('created_at', 'DESC');
//        foreach ($attributes as $field => $value) {
//            $query = $query->where($field, $value);
//        }
//
//        if (null !== $orderBy) {
//            $query->orderBy($orderBy, $sortOrder);
//        }

        return $query->get();
    }
}
