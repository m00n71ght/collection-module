<?php

namespace Modules\Collection\Repositories\Eloquent;

use Modules\Collection\Repositories\OriginCountryRepository;
use Modules\Collection\Repositories\Traits\Importable;
use Modules\Collection\Repositories\Traits\ReferenceBook;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentOriginCountryRepository extends EloquentBaseRepository implements OriginCountryRepository
{
    use Importable, ReferenceBook;
}
