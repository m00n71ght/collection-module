<?php

namespace Modules\Collection\Repositories;


use Illuminate\Http\Request;
use Modules\Core\Repositories\BaseRepository;

interface ItemRepository extends BaseRepository
{
    public function search(Request $request, $featured);
}
