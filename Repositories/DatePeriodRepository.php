<?php

namespace Modules\Collection\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface DatePeriodRepository extends BaseRepository, ReferenceBookInterface
{
    public function updateFromImport(array $data);
}
