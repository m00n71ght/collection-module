<?php

namespace Modules\Collection\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface SectionRepository extends BaseRepository, ReferenceBookInterface
{
    public function updateFromImport(array $data);
}
