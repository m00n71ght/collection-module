<?php


namespace Modules\Collection\Repositories\Traits;


trait Importable
{
    public function updateFromImport(array $data)
    {
        $object = $this->model->firstOrNew(['name' => $data['en']['name']]);
        $object->fill($data);
        $object->save();

        return $object;
    }
}
