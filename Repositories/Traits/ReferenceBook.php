<?php


namespace Modules\Collection\Repositories\Traits;


trait ReferenceBook
{
    public function allActive()
    {
        return $this->allWithBuilder()->active()->orderByDesc('created_at')->get();
    }
}
