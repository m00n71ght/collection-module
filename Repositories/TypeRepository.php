<?php

namespace Modules\Collection\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface TypeRepository extends BaseRepository, ReferenceBookInterface
{
    public function updateFromImport(array $data);
}
