<?php


namespace Modules\Collection\Repositories;


interface ReferenceBookInterface
{
    public function allActive();
}
