<?php

namespace Modules\Collection\Repositories\Cache;

use Modules\Collection\Repositories\DatePeriodRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheDatePeriodDecorator extends BaseCacheDecorator implements DatePeriodRepository
{
    public function __construct(DatePeriodRepository $dateperiod)
    {
        parent::__construct();
        $this->entityName = 'collection.dateperiods';
        $this->repository = $dateperiod;
    }
}
