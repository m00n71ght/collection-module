<?php

namespace Modules\Collection\Repositories\Cache;

use Modules\Collection\Repositories\TypeRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheTypeDecorator extends BaseCacheDecorator implements TypeRepository
{
    public function __construct(TypeRepository $type)
    {
        parent::__construct();
        $this->entityName = 'collection.types';
        $this->repository = $type;
    }
}
