<?php

namespace Modules\Collection\Repositories\Cache;

use Modules\Collection\Repositories\ItemRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheItemDecorator extends BaseCacheDecorator implements ItemRepository
{
    public function __construct(ItemRepository $item)
    {
        parent::__construct();
        $this->entityName = 'collection.items';
        $this->repository = $item;
    }
}
