<?php

namespace Modules\Collection\Repositories\Cache;

use Modules\Collection\Repositories\OriginCountryRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheOriginCountryDecorator extends BaseCacheDecorator implements OriginCountryRepository
{
    public function __construct(OriginCountryRepository $origincountry)
    {
        parent::__construct();
        $this->entityName = 'collection.origincountries';
        $this->repository = $origincountry;
    }
}
