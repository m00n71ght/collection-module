<?php

namespace Modules\Collection\Repositories\Cache;

use Modules\Collection\Repositories\ReferenceBookRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheReferenceBookDecorator extends BaseCacheDecorator implements ReferenceBookRepository
{
    public function __construct(ReferenceBookRepository $referencebook)
    {
        parent::__construct();
        $this->entityName = 'collection.referencebooks';
        $this->repository = $referencebook;
    }
}
