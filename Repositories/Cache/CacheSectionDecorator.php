<?php

namespace Modules\Collection\Repositories\Cache;

use Modules\Collection\Repositories\SectionRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheSectionDecorator extends BaseCacheDecorator implements SectionRepository
{
    public function __construct(SectionRepository $section)
    {
        parent::__construct();
        $this->entityName = 'collection.sections';
        $this->repository = $section;
    }
}
