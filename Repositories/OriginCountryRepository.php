<?php

namespace Modules\Collection\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface OriginCountryRepository extends BaseRepository, ReferenceBookInterface
{
    public function updateFromImport(array $data);
}
