<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/collection'], function (Router $router) {
    $router->bind('item', function ($id) {
        return app('Modules\Collection\Repositories\ItemRepository')->find($id);
    });
    $router->get('items', [
        'as' => 'admin.collection.item.index',
        'uses' => 'ItemController@index',
        'middleware' => 'can:collection.items.index'
    ]);
    $router->get('items/create', [
        'as' => 'admin.collection.item.create',
        'uses' => 'ItemController@create',
        'middleware' => 'can:collection.items.create'
    ]);
    $router->post('items', [
        'as' => 'admin.collection.item.store',
        'uses' => 'ItemController@store',
        'middleware' => 'can:collection.items.create'
    ]);
    $router->get('items/{item}/edit', [
        'as' => 'admin.collection.item.edit',
        'uses' => 'ItemController@edit',
        'middleware' => 'can:collection.items.edit'
    ]);
    $router->put('items/{item}', [
        'as' => 'admin.collection.item.update',
        'uses' => 'ItemController@update',
        'middleware' => 'can:collection.items.edit'
    ]);
    $router->delete('items/{item}', [
        'as' => 'admin.collection.item.destroy',
        'uses' => 'ItemController@destroy',
        'middleware' => 'can:collection.items.destroy'
    ]);
    $router->bind('dateperiod', function ($id) {
        return app('Modules\Collection\Repositories\DatePeriodRepository')->find($id);
    });
    $router->get('dateperiods', [
        'as' => 'admin.collection.dateperiod.index',
        'uses' => 'DatePeriodController@index',
        'middleware' => 'can:collection.dateperiods.index'
    ]);
    $router->get('dateperiods/create', [
        'as' => 'admin.collection.dateperiod.create',
        'uses' => 'DatePeriodController@create',
        'middleware' => 'can:collection.dateperiods.create'
    ]);
    $router->post('dateperiods', [
        'as' => 'admin.collection.dateperiod.store',
        'uses' => 'DatePeriodController@store',
        'middleware' => 'can:collection.dateperiods.create'
    ]);
    $router->get('dateperiods/{dateperiod}/edit', [
        'as' => 'admin.collection.dateperiod.edit',
        'uses' => 'DatePeriodController@edit',
        'middleware' => 'can:collection.dateperiods.edit'
    ]);
    $router->put('dateperiods/{dateperiod}', [
        'as' => 'admin.collection.dateperiod.update',
        'uses' => 'DatePeriodController@update',
        'middleware' => 'can:collection.dateperiods.edit'
    ]);
    $router->delete('dateperiods/{dateperiod}', [
        'as' => 'admin.collection.dateperiod.destroy',
        'uses' => 'DatePeriodController@destroy',
        'middleware' => 'can:collection.dateperiods.destroy'
    ]);
    $router->bind('origincountry', function ($id) {
        return app('Modules\Collection\Repositories\OriginCountryRepository')->find($id);
    });
    $router->get('origincountries', [
        'as' => 'admin.collection.origincountry.index',
        'uses' => 'OriginCountryController@index',
        'middleware' => 'can:collection.origincountries.index'
    ]);
    $router->get('origincountries/create', [
        'as' => 'admin.collection.origincountry.create',
        'uses' => 'OriginCountryController@create',
        'middleware' => 'can:collection.origincountries.create'
    ]);
    $router->post('origincountries', [
        'as' => 'admin.collection.origincountry.store',
        'uses' => 'OriginCountryController@store',
        'middleware' => 'can:collection.origincountries.create'
    ]);
    $router->get('origincountries/{origincountry}/edit', [
        'as' => 'admin.collection.origincountry.edit',
        'uses' => 'OriginCountryController@edit',
        'middleware' => 'can:collection.origincountries.edit'
    ]);
    $router->put('origincountries/{origincountry}', [
        'as' => 'admin.collection.origincountry.update',
        'uses' => 'OriginCountryController@update',
        'middleware' => 'can:collection.origincountries.edit'
    ]);
    $router->delete('origincountries/{origincountry}', [
        'as' => 'admin.collection.origincountry.destroy',
        'uses' => 'OriginCountryController@destroy',
        'middleware' => 'can:collection.origincountries.destroy'
    ]);
    $router->bind('type', function ($id) {
        return app('Modules\Collection\Repositories\TypeRepository')->find($id);
    });
    $router->get('types', [
        'as' => 'admin.collection.type.index',
        'uses' => 'TypeController@index',
        'middleware' => 'can:collection.types.index'
    ]);
    $router->get('types/create', [
        'as' => 'admin.collection.type.create',
        'uses' => 'TypeController@create',
        'middleware' => 'can:collection.types.create'
    ]);
    $router->post('types', [
        'as' => 'admin.collection.type.store',
        'uses' => 'TypeController@store',
        'middleware' => 'can:collection.types.create'
    ]);
    $router->get('types/{type}/edit', [
        'as' => 'admin.collection.type.edit',
        'uses' => 'TypeController@edit',
        'middleware' => 'can:collection.types.edit'
    ]);
    $router->put('types/{type}', [
        'as' => 'admin.collection.type.update',
        'uses' => 'TypeController@update',
        'middleware' => 'can:collection.types.edit'
    ]);
    $router->delete('types/{type}', [
        'as' => 'admin.collection.type.destroy',
        'uses' => 'TypeController@destroy',
        'middleware' => 'can:collection.types.destroy'
    ]);
    $router->bind('section', function ($id) {
        return app('Modules\Collection\Repositories\SectionRepository')->find($id);
    });
    $router->get('sections', [
        'as' => 'admin.collection.section.index',
        'uses' => 'SectionController@index',
        'middleware' => 'can:collection.sections.index'
    ]);
    $router->get('sections/create', [
        'as' => 'admin.collection.section.create',
        'uses' => 'SectionController@create',
        'middleware' => 'can:collection.sections.create'
    ]);
    $router->post('sections', [
        'as' => 'admin.collection.section.store',
        'uses' => 'SectionController@store',
        'middleware' => 'can:collection.sections.create'
    ]);
    $router->get('sections/{section}/edit', [
        'as' => 'admin.collection.section.edit',
        'uses' => 'SectionController@edit',
        'middleware' => 'can:collection.sections.edit'
    ]);
    $router->put('sections/{section}', [
        'as' => 'admin.collection.section.update',
        'uses' => 'SectionController@update',
        'middleware' => 'can:collection.sections.edit'
    ]);
    $router->delete('sections/{section}', [
        'as' => 'admin.collection.section.destroy',
        'uses' => 'SectionController@destroy',
        'middleware' => 'can:collection.sections.destroy'
    ]);

    $router->post('sections/import', [
        'uses' => 'SectionController@import',
        'as' => 'admin.collection.section.import',
        'middleware' => 'can:collection.sections.import',
    ]);

    $router->post('types/import', [
        'uses' => 'TypeController@import',
        'as' => 'admin.collection.type.import',
        'middleware' => 'can:collection.types.import',
    ]);

    $router->post('dateperiods/import', [
        'uses' => 'DatePeriodController@import',
        'as' => 'admin.collection.dateperiod.import',
        'middleware' => 'can:collection.dateperiods.import',
    ]);

    $router->post('origincountries/import', [
        'uses' => 'OriginCountryController@import',
        'as' => 'admin.collection.origincountry.import',
        'middleware' => 'can:collection.origincountries.import',
    ]);
// append






});
