<?php

namespace Modules\Collection\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Collection\Repositories\ItemRepository;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Menu\Repositories\MenuItemRepository;
use Modules\Page\Entities\Page;
use Modules\Page\Repositories\PageRepository;

class PublicController extends BasePublicController
{
    /**
     * @var PageRepository
     */
    private $item;
    /**
     * @var Application
     */
    private $app;

    public function __construct(ItemRepository $item, Application $app)
    {
        parent::__construct();
        $this->item = $item;
        $this->app = $app;
    }

    /**
     * @param $slug
     * @return \Illuminate\View\View
     */
    public function item($slug)
    {
        $item = $this->item->findBySlug($slug);

        if (!$item) {
            $this->app->abort('404');
        }
        $item->load('files', 'section', 'origincountry', 'type', 'dateperiod');

        $currentTranslatedPage = $item->getTranslation(locale());
        if ($slug !== $currentTranslatedPage->slug) {

            return redirect()->to($currentTranslatedPage->locale . '/collection/item/' . $currentTranslatedPage->slug, 301);
        }

        $template = 'collection.item';

        return view($template, compact('item'));
    }
}
