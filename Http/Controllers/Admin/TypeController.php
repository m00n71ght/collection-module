<?php

namespace Modules\Collection\Http\Controllers\Admin;

use Illuminate\Http\Response;
use Modules\Collection\Entities\Type;
use Modules\Collection\Http\Requests\CreateTypeRequest;
use Modules\Collection\Http\Requests\UpdateTypeRequest;
use Modules\Collection\Importers\ReferenceBookImporter;
use Modules\Collection\Repositories\TypeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Collection\Http\Requests\ImportReferenceBookRequest;

class TypeController extends AdminBaseController
{
    /**
     * @var TypeRepository
     */
    private $type;

    public function __construct(TypeRepository $type)
    {
        parent::__construct();

        $this->type = $type;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $types = $this->type->all();

        return view('collection::admin.types.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('collection::admin.types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateTypeRequest $request
     * @return Response
     */
    public function store(CreateTypeRequest $request)
    {
        $this->type->create($request->all());

        return redirect()->route('admin.collection.type.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('collection::types.title.types')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Type $type
     * @return Response
     */
    public function edit(Type $type)
    {
        return view('collection::admin.types.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Type $type
     * @param  UpdateTypeRequest $request
     * @return Response
     */
    public function update(Type $type, UpdateTypeRequest $request)
    {
        $this->type->update($type, $request->all());

        return redirect()->route('admin.collection.type.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('collection::types.title.types')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Type $type
     * @return Response
     */
    public function destroy(Type $type)
    {
        $this->type->destroy($type);

        return redirect()->route('admin.collection.type.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('collection::types.title.types')]));
    }

    public function import(ImportReferenceBookRequest $request)
    {
        $importer = new ReferenceBookImporter($this->type);
        $importer->import($request->file('file'));

        return redirect()->route('admin.collection.type.index')->withSuccess(trans('collection::types.imported'));
    }
}
