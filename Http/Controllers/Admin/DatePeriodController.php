<?php

namespace Modules\Collection\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Collection\Entities\Dateperiod;
use Modules\Collection\Http\Requests\CreateDatePeriodRequest;
use Modules\Collection\Http\Requests\ImportReferenceBookRequest;
use Modules\Collection\Http\Requests\UpdateDatePeriodRequest;
use Modules\Collection\Importers\ReferenceBookImporter;
use Modules\Collection\Repositories\DatePeriodRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class DatePeriodController extends AdminBaseController
{
    /**
     * @var DatePeriodRepository
     */
    private $dateperiod;

    public function __construct(DatePeriodRepository $dateperiod)
    {
        parent::__construct();

        $this->dateperiod = $dateperiod;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $dateperiods = $this->dateperiod->all();

        return view('collection::admin.dateperiods.index', compact('dateperiods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('collection::admin.dateperiods.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateDatePeriodRequest $request
     * @return Response
     */
    public function store(CreateDatePeriodRequest $request)
    {
        $this->dateperiod->create($request->all());

        return redirect()->route('admin.collection.dateperiod.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('collection::dateperiods.title.dateperiods')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Dateperiod $dateperiod
     * @return Response
     */
    public function edit(Dateperiod $dateperiod)
    {
        return view('collection::admin.dateperiods.edit', compact('dateperiod'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Dateperiod $dateperiod
     * @param  UpdateDatePeriodRequest $request
     * @return Response
     */
    public function update(Dateperiod $dateperiod, UpdateDatePeriodRequest $request)
    {
        $this->dateperiod->update($dateperiod, $request->all());

        return redirect()->route('admin.collection.dateperiod.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('collection::dateperiods.title.dateperiods')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Dateperiod $dateperiod
     * @return Response
     */
    public function destroy(Dateperiod $dateperiod)
    {
        $this->dateperiod->destroy($dateperiod);

        return redirect()->route('admin.collection.dateperiod.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('collection::dateperiods.title.dateperiods')]));
    }

    public function import(ImportReferenceBookRequest $request)
    {
        $importer = new ReferenceBookImporter($this->dateperiod);
        $importer->import($request->file('file'));

        return redirect()->route('admin.collection.dateperiod.index')->withSuccess(trans('collection::types.imported'));
    }
}
