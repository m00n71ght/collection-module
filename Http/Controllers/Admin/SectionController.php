<?php

namespace Modules\Collection\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Collection\Entities\Section;
use Modules\Collection\Http\Requests\CreateSectionRequest;
use Modules\Collection\Http\Requests\UpdateSectionRequest;
use Modules\Collection\Importers\ReferenceBookImporter;
use Modules\Collection\Repositories\SectionRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Collection\Http\Requests\ImportReferenceBookRequest;

class SectionController extends AdminBaseController
{
    /**
     * @var SectionRepository
     */
    private $section;

    public function __construct(SectionRepository $section)
    {
        parent::__construct();

        $this->section = $section;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $sections = $this->section->all();

        return view('collection::admin.sections.index', compact('sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('collection::admin.sections.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateSectionRequest $request
     * @return Response
     */
    public function store(CreateSectionRequest $request)
    {
        $this->section->create($request->all());

        return redirect()->route('admin.collection.section.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('collection::sections.title.sections')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Section $section
     * @return Response
     */
    public function edit(Section $section)
    {
        return view('collection::admin.sections.edit', compact('section'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Section $section
     * @param  UpdateSectionRequest $request
     * @return Response
     */
    public function update(Section $section, UpdateSectionRequest $request)
    {
        $this->section->update($section, $request->all());

        return redirect()->route('admin.collection.section.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('collection::sections.title.sections')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Section $section
     * @return Response
     */
    public function destroy(Section $section)
    {
        $this->section->destroy($section);

        return redirect()->route('admin.collection.section.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('collection::sections.title.sections')]));
    }

    public function import(ImportReferenceBookRequest $request)
    {
        $importer = new ReferenceBookImporter($this->section);
        $importer->import($request->file('file'));

        return redirect()->route('admin.collection.section.index')->withSuccess(trans('collection::types.imported'));
    }
}
