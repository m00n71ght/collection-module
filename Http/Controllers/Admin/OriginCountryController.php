<?php

namespace Modules\Collection\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Collection\Entities\Origincountry;
use Modules\Collection\Http\Requests\CreateOriginCountryRequest;
use Modules\Collection\Http\Requests\ImportReferenceBookRequest;
use Modules\Collection\Http\Requests\UpdateOriginCountryRequest;
use Modules\Collection\Importers\ReferenceBookImporter;
use Modules\Collection\Repositories\OriginCountryRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class OriginCountryController extends AdminBaseController
{
    /**
     * @var OriginCountryRepository
     */
    private $origincountry;

    public function __construct(OriginCountryRepository $origincountry)
    {
        parent::__construct();

        $this->origincountry = $origincountry;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $origincountries = $this->origincountry->all();

        return view('collection::admin.origincountries.index', compact('origincountries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('collection::admin.origincountries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateOriginCountryRequest $request
     * @return Response
     */
    public function store(CreateOriginCountryRequest $request)
    {
        $this->origincountry->create($request->all());

        return redirect()->route('admin.collection.origincountry.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('collection::origincountries.title.origincountries')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Origincountry $origincountry
     * @return Response
     */
    public function edit(Origincountry $origincountry)
    {
        return view('collection::admin.origincountries.edit', compact('origincountry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Origincountry $origincountry
     * @param  UpdateOriginCountryRequest $request
     * @return Response
     */
    public function update(Origincountry $origincountry, UpdateOriginCountryRequest $request)
    {
        $this->origincountry->update($origincountry, $request->all());

        return redirect()->route('admin.collection.origincountry.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('collection::origincountries.title.origincountries')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Origincountry $origincountry
     * @return Response
     */
    public function destroy(Origincountry $origincountry)
    {
        $this->origincountry->destroy($origincountry);

        return redirect()->route('admin.collection.origincountry.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('collection::origincountries.title.origincountries')]));
    }

    public function import(ImportReferenceBookRequest $request)
    {
        $importer = new ReferenceBookImporter($this->origincountry);
        $importer->import($request->file('file'));

        return redirect()->route('admin.collection.origincountry.index')->withSuccess(trans('collection::types.imported'));
    }
}
