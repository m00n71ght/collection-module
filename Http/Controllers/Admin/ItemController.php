<?php

namespace Modules\Collection\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Lang;
use Modules\Collection\Entities\Item;
use Modules\Collection\Entities\Section;
use Modules\Collection\Http\Requests\CreateItemRequest;
use Modules\Collection\Http\Requests\UpdateItemRequest;
use Modules\Collection\Repositories\DatePeriodRepository;
use Modules\Collection\Repositories\ItemRepository;
use Modules\Collection\Repositories\OriginCountryRepository;
use Modules\Collection\Repositories\SectionRepository;
use Modules\Collection\Repositories\TypeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class ItemController extends AdminBaseController
{
    /**
     * @var ItemRepository
     */
    private $item;
    private $section;
    private $type;
    private $date_period;
    private $origin_country;

    public function __construct(ItemRepository $item, SectionRepository $section, TypeRepository $type, DatePeriodRepository $date_period, OriginCountryRepository $origin_country)
    {
        parent::__construct();

        $this->item = $item;
        $this->section = $section;
        $this->type = $type;
        $this->date_period = $date_period;
        $this->origin_country = $origin_country;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items = $this->item->all();

        return view('collection::admin.items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $sections = $this->section->allTranslatedIn(Lang::locale())->pluck('name', 'id')->toArray();
        $types = $this->type->allTranslatedIn(Lang::locale())->pluck('name', 'id')->toArray();
        $date_periods = $this->date_period->allTranslatedIn(Lang::locale())->pluck('name', 'id')->toArray();
        $origin_countries = $this->origin_country->allTranslatedIn(Lang::locale())->pluck('name', 'id')->toArray();
        return view('collection::admin.items.create', compact('sections', 'types', 'date_periods', 'origin_countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateItemRequest $request
     * @return Response
     */
    public function store(CreateItemRequest $request)
    {
        $this->item->create($request->all());

        return redirect()->route('admin.collection.item.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('collection::items.title.items')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Item $item
     * @return Response
     */
    public function edit(Item $item)
    {
        $sections = $this->section->allTranslatedIn(Lang::locale())->pluck('name', 'id')->toArray();
        $types = $this->type->allTranslatedIn(Lang::locale())->pluck('name', 'id')->toArray();
        $date_periods = $this->date_period->allTranslatedIn(Lang::locale())->pluck('name', 'id')->toArray();
        $origin_countries = $this->origin_country->allTranslatedIn(Lang::locale())->pluck('name', 'id')->toArray();
        return view('collection::admin.items.edit', compact('item', 'sections', 'types', 'date_periods', 'origin_countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Item $item
     * @param  UpdateItemRequest $request
     * @return Response
     */
    public function update(Item $item, UpdateItemRequest $request)
    {
        $this->item->update($item, $request->all());

        return redirect()->route('admin.collection.item.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('collection::items.title.items')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Item $item
     * @return Response
     */
    public function destroy(Item $item)
    {
        $this->item->destroy($item);

        return redirect()->route('admin.collection.item.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('collection::items.title.items')]));
    }
}
