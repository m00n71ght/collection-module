<?php

namespace Modules\Collection\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateItemRequest extends BaseFormRequest
{
    public function rules()
    {
        return [];
    }

    public function translationRules()
    {
        return [
//            'author' => 'required',
            'name' => 'required',
//            'material' => 'required',
//            'size' => 'required',
//            'inventory_number' => 'required',
//            'source' => 'required',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [
            'author.required' => trans('collection::messages.author is required'),
            'name.required' => trans('collection::messages.name is required'),
            'slug.required' => trans('collection::messages.slug is required'),
            'material.required' => trans('collection::messages.material is required'),
            'size.required' => trans('collection::messages.size is required'),
            'inventory_number.required' => trans('collection::messages.inventory_number is required'),
            'source.required' => trans('collection::messages.source is required'),
        ];
    }
}
