<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->get('collection/item/{slug}', [
    'as' => 'collection.item',
    'uses' => 'PublicController@item',
    'middleware' => config('asgard.page.config.middleware'),
]);
