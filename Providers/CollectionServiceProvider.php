<?php

namespace Modules\Collection\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Collection\Events\Handlers\RegisterCollectionSidebar;

class CollectionServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterCollectionSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('items', array_dot(trans('collection::items')));
            $event->load('dateperiods', array_dot(trans('collection::dateperiods')));
            $event->load('origincountries', array_dot(trans('collection::origincountries')));
            $event->load('types', array_dot(trans('collection::types')));
            $event->load('sections', array_dot(trans('collection::sections')));
            // append translations






        });
    }

    public function boot()
    {
        $this->publishConfig('collection', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Collection\Repositories\ItemRepository',
            function () {
                $repository = new \Modules\Collection\Repositories\Eloquent\EloquentItemRepository(new \Modules\Collection\Entities\Item());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Collection\Repositories\Cache\CacheItemDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Collection\Repositories\DatePeriodRepository',
            function () {
                $repository = new \Modules\Collection\Repositories\Eloquent\EloquentDatePeriodRepository(new \Modules\Collection\Entities\Dateperiod());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Collection\Repositories\Cache\CacheDatePeriodDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Collection\Repositories\OriginCountryRepository',
            function () {
                $repository = new \Modules\Collection\Repositories\Eloquent\EloquentOriginCountryRepository(new \Modules\Collection\Entities\Origincountry());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Collection\Repositories\Cache\CacheOriginCountryDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Collection\Repositories\TypeRepository',
            function () {
                $repository = new \Modules\Collection\Repositories\Eloquent\EloquentTypeRepository(new \Modules\Collection\Entities\Type());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Collection\Repositories\Cache\CacheTypeDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Collection\Repositories\SectionRepository',
            function () {
                $repository = new \Modules\Collection\Repositories\Eloquent\EloquentSectionRepository(new \Modules\Collection\Entities\Section());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Collection\Repositories\Cache\CacheSectionDecorator($repository);
            }
        );
// add bindings






    }
}
