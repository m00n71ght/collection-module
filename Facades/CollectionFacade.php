<?php


namespace Modules\Collection\Facades;


use Illuminate\Support\Facades\Facade;
use Modules\Collection\Presenters\CollectionPresenter;

class CollectionFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return CollectionPresenter::class;
    }
}
